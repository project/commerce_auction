<?php

/**
 * @file
 * Contains core functions of reverse auction module.
 */

/**
 * Implements hook_views_api().
 */
function commerce_reverse_auction_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_commerce_line_item_type_info().
 */
function commerce_reverse_auction_commerce_line_item_type_info() {
  $line_item_types = array();

  $line_item_types['commerce_reverse_auction_lineitem'] = array(
    'name' => t('Reverse auction Line Item'),
    'description' => t('Line Item Type representing a reverse auctioned product'),
    'product' => TRUE,
    'add_form_submit_value' => t('Add reverse auction product'),
    'base' => 'commerce_product_line_item',
  );

  return $line_item_types;
}

/**
 * Implements hook_menu().
 */
function commerce_reverse_auction_menu() {
  $items = array();

  $items['admin/commerce/auction/auction'] = array(
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'title' => 'Auction',
  );
  $items['admin/commerce/auction/reverse_auction'] = array(
    'title' => 'Reverse Auction',
    'description' => 'Configure commerce reverse auction behavior and display node types',
    'access arguments' => array('administer commerce reverse auction'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_reverse_auction_admin_form'),
    'file' => 'includes/commerce_reverse_auction.admin.inc',
    'type' => MENU_LOCAL_TASK,
  );
  $items['node/%node/placerbid'] = array(
    'title' => 'Place bid',
    'type' => MENU_LOCAL_TASK,
    'access callback' => 'commerce_reverse_auction_place_bid_access_callback',
    'access arguments' => array(1, 'place bids on reverse auctions'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_reverse_auction_place_bid_form', 1),
    'file' => 'includes/commerce_reverse_auction.pages.inc',
  );
  $items['node/%node/editbid'] = array(
    'title' => 'Edit your bid',
    'type' => MENU_LOCAL_TASK,
    'access callback' => 'commerce_reverse_auction_edit_bid_access_callback',
    'access arguments' => array(1, 'edit own bids on reverse auctions'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_reverse_auction_edit_bid_form', 1),
    'file' => 'includes/commerce_reverse_auction.pages.inc',
  );
  $items['node/%node/rbids_list'] = array(
    'title' => 'Bids',
    'type' => MENU_LOCAL_TASK,
    'access callback' => 'commerce_reverse_auction_bids_list_access_callback',
    'access arguments' => array(1),
    'page callback' => 'commerce_reverse_auction_bids_list',
    'page arguments' => array(1),
  );
  $items['node/%node/bid/%'] = array(
    'title' => 'Bid details',
    'access callback' => 'commerce_reverse_auction_view_bid_access_callback',
    'access arguments' => array(1, 3),
    'page callback' => 'commerce_reverse_auction_view_bid',
    'page arguments' => array(3),
  );
  $items['node/%node/bid/%/accept'] = array(
    'title' => 'Accept bid',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_reverse_auction_accept_bid_confirm', 1, 3),
    'access callback' => 'commerce_reverse_auction_accept_bid_access_callback',
    'access arguments' => array(1, 3),
    'file' => 'includes/commerce_reverse_auction.pages.inc',
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function commerce_reverse_auction_permission() {
  return array(
    'place bids on reverse auctions' =>  array(
      'title' => t('Place bids on reverse auctions'),
    ),
    'administer commerce reverse auction' => array(
      'title' => t('Change Commerce reverse auction options'),
    ),
    'delete own bid from reverse auction' => array(
      'title' => t('Delete own bid from a reverse auction (while its still open)'),
    ),
    'edit own bids on reverse auctions' => array(
      'title' => t('Edit own bid on a reverse auction'),
    ),
    'remove won reverse auctions from cart' => array(
      'title' => t('Remove accepted reverse auctioned products from own cart')
    ),
  );
}

/**
 * Access callback for placing bids on reverse auctions.
 */
function commerce_reverse_auction_place_bid_access_callback($node, $access_arg) {
  if (!user_access($access_arg)) {
    return FALSE;
  }

  global $user;
  if ($user->uid == $node->uid) {
    return FALSE;
  }
  $types = variable_get('commerce_reverse_auction_display_types', array());
  if (!in_array($node->type, $types)) {
    return FALSE;
  }
  // See if the user has already placed a bid on the auction. If so do not allow
  // him to place another bid, She'll have to edit his previous bid.
  $query = new EntityFieldQuery();
  // Entity type is commerce_auction_bid
  $query->entityCondition('entity_type', 'commerce_auction_bid')
  // Bundle is the reverse_auction_bid
  ->entityCondition('bundle', 'reverse_auction_bid')
  // Owner is the same as the currently logged in user,
  ->propertyCondition('uid', $user->uid, '=')
  // reverse_auction_ref referencing this node,
  ->fieldCondition('reverse_auction_ref', 'target_id', $node->nid, '=');
  $result = $query->execute();
  if (!empty($result)) {
    return FALSE;
  }
  $timeout_field = field_get_items('node', $node, 'auction_timeout');
  if (!isset($timeout_field) ||
      empty($timeout_field) ||
      !$timeout_field[0]['value']) {
    drupal_set_message(
      t('Auction timeout has no value, Please set a value for the timeout field.'),
      'warning'
    );
    return FALSE;
  }

  $expiration_timestamp = $timeout_field[0]['value'];
  if ($expiration_timestamp <= REQUEST_TIME) {
    return FALSE;
  }

  return TRUE;
}

/**
 * Access callback for edit own bid page.
 */
function commerce_reverse_auction_edit_bid_access_callback($node) {
  $types = variable_get('commerce_reverse_auction_display_types', array());
  if (!in_array($node->type, $types)) {
    return FALSE;
  }

  global $user;
  // See if the user has already placed a bid on the auction. If so do not allow
  // him to place another bid, She'll have to edit his previous bid.
  $query = new EntityFieldQuery();
  // Entity type is commerce_auction_bid
  $query->entityCondition('entity_type', 'commerce_auction_bid')
  // Bundle is the reverse_auction_bid
  ->entityCondition('bundle', 'reverse_auction_bid')
  // Owner is the same as the currently logged in user,
  ->propertyCondition('uid', $user->uid, '=')
  // reverse_auction_ref referencing this node,
  ->fieldCondition('reverse_auction_ref', 'target_id', $node->nid, '=');
  $result = $query->execute();
  if (empty($result)) {
    return FALSE;
  }
  $timeout_field = field_get_items('node', $node, 'auction_timeout');
  if (!isset($timeout_field) ||
      empty($timeout_field) ||
      !$timeout_field[0]['value']) {
    return FALSE;
  }
  $expiration_timestamp = $timeout_field[0]['value'];
  if ($expiration_timestamp <= REQUEST_TIME) {
    return FALSE;
  }
  return TRUE;
}

/**
 * Access callback for bids list menu item.
 */
function commerce_reverse_auction_bids_list_access_callback($node) {
  $types = variable_get('commerce_reverse_auction_display_types', array());
  if (!in_array($node->type, $types)) {
    return FALSE;
  }
  if (!user_access('eck view commerce_auction_bid reverse_auction_bid entities')) {
    return FALSE;
  }
  return TRUE;
}

/**
 * Page callback for bids list menu item.
 */
function commerce_reverse_auction_bids_list($node) {
  return commerce_embed_view('reverse_auction_bid_list', 'master', array($node->nid));
}

/**
 * Access callback for bid details page.
 */
function commerce_reverse_auction_view_bid_access_callback($node, $bid_id) {
  return TRUE;
}

/**
 * Page callback for bid details page.
 */
function commerce_reverse_auction_view_bid($bid_id) {
  return commerce_embed_view('reverse_auction_bid_details', 'master', array($bid_id));
}

/**
 * This function reverse auction related removes fields from content types.
 *
 * @param array $old_types
 *   The auction display content types before updating. Auction related fields
 *   will be removed from content types in this array if those types are not in
 *   $new_types.
 *
 * @param array $new_types
 *   The new auction display content types.
 */
function commerce_reverse_auction_remove_fields($old_types, $new_types = array()) {
  $field_names = array(
    'auction_timeout',
    'auction_min_price',
    'auction_max_price',
    'bid_product',
  );
  foreach ($old_types as $type) {
    if (in_array($type, $new_types) === FALSE) {
      // $type has been removed from auction display types,
      // remove unused fields.
      foreach ($field_names as $field_name) {
        $field = field_info_instance('node', $field_name, $type);
        if ($field) {
          field_delete_instance($field);
        }
      }
    }
  }
}

/**
 * Implements hook_theme().
 */
function commerce_reverse_auction_theme(){
  $theme = array();

  $theme['commerce_auction_bid__reverse_auction_bid'] = array(
    'render element' => 'elements',
    'template' => 'commerce_auction_bid--reverse_auction_bid',
  );

  return $theme;
}

/**
 * Implements template_preprocess_entity().
 */
function commerce_reverse_auction_preprocess_entity(&$variables) {
  if ($variables['elements']['#entity']->type == 'reverse_auction_bid') {
    $referenced_node = field_get_items('commerce_auction_bid', $variables['elements']['#entity'], 'reverse_auction_ref');
    global $user;
    $node = node_load($referenced_node[0]['target_id']);
    $product_field = field_get_items('node', $node, 'field_product');

    if ($node->uid == $user->uid && !(isset($product_field[0]['product_id']) && $product_field[0]['product_id'] > 0)) {
      $variables['content']['links']['#accept_link'] = array(
        '#markup' => l(t('Accept this bid'), 'node/' . $node->nid . '/bid/' . $variables['elements']['#entity']->id . '/accept'),
      );
    }

    $variables['author'] = user_load($variables['elements']['#entity']->uid);
    $variables['title'] = t('Bid by @user', array());
    $variables['created_info'] = t('On @date by !user', array('@date' => format_date($variables['elements']['#entity']->created), '!user' => theme('username', array('account' => $variables['author']))));
  }
}

/**
 * Access callback for accept bid page.
 */
function commerce_reverse_auction_accept_bid_access_callback($node, $bid_id) {
  $product_field = field_get_items('node', $node, 'field_product');
  if (isset($product_field[0]['product_id']) && $product_field[0]['product_id'] > 0) {
    return FALSE;
  }

  $entity = entity_load('commerce_auction_bid', array($bid_id));
  if (!isset($entity[$bid_id])) {
    return FALSE;
  }
  $entity = $entity[$bid_id];
  $referenced_node = field_get_items('commerce_auction_bid', $entity, 'reverse_auction_ref');
  if ($referenced_node[0]['target_id'] != $node->nid) {
    return FALSE;
  }

  global $user;
  if ($node->uid != $user->uid) {
    return FALSE;
  }
  return TRUE;
}

/**
 * Implements hook_form_alter().
 */
function commerce_reverse_auction_form_alter(&$form, &$form_state, $form_id) {
  $types = variable_get('commerce_reverse_auction_display_types', array());
  if ($form_id == 'views_form_commerce_cart_form_default') {
    foreach (array('edit_delete', 'edit_quantity') as $form_key) {
      foreach ($form[$form_key] as $key => $item) {
        if (is_array($item)) {
          $line_item = commerce_line_item_load($item['#line_item_id']);
          if ($line_item->type == 'commerce_reverse_auction_lineitem' && !user_access('remove won reverse auctions from cart')) {
            $form[$form_key][$key]['#disabled'] = TRUE;
          }
        }
      }
    }
  }
}
