<?php

namespace Drupal\commerce_auction\Tests;

use \CommerceBaseTestCase;

/**
 * Base class for Commerce Auction tests.
 */
abstract class TestBase extends CommerceBaseTestCase {
  /**
   * The number of seconds in one day.
   *
   * Useful for setting auction timeouts.
   *
   * @var int
   */
  const DAY_IN_SECONDS = 86400;

  /**
   * An account with admin rights.
   *
   * @var object
   */
  protected $adminAccount;

  /**
   * Product type definition.
   *
   * @var array
   */
  protected $productType;

  /**
   * Content ype.
   *
   * @var object
   */
  protected $contentType;

  /**
   * Overrides DrupalWebTestCase::setUp().
   *
   * @param array $modules
   *   (optional) A list of modules to enable.
   * @param array $permissions
   *   (optional) The permissions for the admin user.
   */
  public function setUp(array $modules = array(), array $permissions = array()) {
    $modules = array_merge($modules, $this->setUpHelper('ui'), array('commerce_auction'));
    parent::setUp($modules);

    // Use permissions builder.
    $permissions = array_unique(array_merge($permissions, $this->permissionBuilder(array(
      'site admin',
      'store admin',
      'auction admin',
    ))));

    // Create an admin user.
    $this->adminAccount = $this->drupalCreateUser($permissions);
    $this->drupalLogin($this->adminAccount);

    // Add a product type.
    $this->productType = $this->createDummyProductType('auction', 'Auction Product', '', '', FALSE);

    // Assert that the product type is shown in the UI.
    $this->drupalGet('admin/commerce/products/types');
    $this->assertText('(Machine name: auction)');

    // Add a content type.
    $this->contentType = $this->drupalCreateContentType(array(
      'type' => 'auction_display',
      'name' => 'Auction Display',
    ));

    // Set content type 'auction_display' as Auction display content type.
    $edit = array(
      'auction_displays[]' => array('auction_display'),
    );
    $this->drupalPost('admin/commerce/auction', $edit, t('Save settings'));
  }

  /**
   * Overrides CommerceBaseTestCase::permissionBuilder().
   *
   * Adds sets for auction permissions.
   */
  protected function permissionBuilder($sets) {
    if (is_string($sets)) {
      $sets = array($sets);
    }

    $auction_admin = array(
      'place bids',
      'remove won auctions from cart',
      'administer commerce auction',
    );
    $auction_customer = array(
      'place bids',
    );

    $final_permissions = parent::permissionBuilder($sets);

    foreach ($sets as $set) {
      switch ($set) {
        case 'auction admin':
          $final_permissions = array_unique(array_merge($final_permissions, $auction_admin));
          break;

        case 'auction customer':
          $final_permissions = array_unique(array_merge($final_permissions, $auction_customer));
          break;
      }
    }

    return $final_permissions;
  }

  /**
   * Creates an auction product.
   *
   * @param string $sku
   *   (optional) The product's SKU.
   * @param string $title
   *   (optional) The product's title.
   * @param int $amount
   *   (optional) The product price.
   *   Defaults to 5000.
   *
   * @return object
   *   A Commerce product.
   */
  protected function createAuctionProduct($sku = '', $title = '', $amount = 5000) {
    $product = $this->createDummyProduct($sku, $title, $amount, 'USD', 1, 'auction');
    commerce_product_save($product);

    return $product;
  }

  /**
   * Creates an auction product display.
   *
   * @param int $product_id
   *   The product to create a display for.
   * @param array $values
   *   (optional) Additional values for product display.
   *
   * @return object
   *   A product node.
   */
  protected function createAuctionProductDisplay($product_id, $values = array()) {
    $values['type'] = 'auction_display';
    $values['field_product'][LANGUAGE_NONE][0]['product_id'] = $product_id;

    // Make sure an auction timeout is set.
    // By default set to one day in the future.
    if (!isset($values['auction_timeout'])) {
      $values['auction_timeout'][LANGUAGE_NONE][0] = array(
        'value' => REQUEST_TIME + static::DAY_IN_SECONDS,
        'timezone_db' => 'UTC',
        'date_type' => 'datestamp',
      );
    }

    return $this->drupalCreateNode($values);
  }

  /**
   * Places a bid.
   *
   * @param int $nid
   *   ID of the auction node to place a bid on.
   * @param float $amount
   *   The amount to bid.
   * @param bool $assert_success
   *   (optional) Whether or not to assert success of the action.
   *   Defaults to TRUE.
   */
  protected function placeBid($nid, $amount, $assert_success = TRUE) {
    // Place a bid.
    $edit = array(
      'bid_amount[und][0][amount]' => $amount,
    );
    $this->drupalPost('node/' . $nid . '/placebid', $edit, t('Place my bid!'));

    if ($assert_success) {
      $this->assertPlaceBid($nid, $amount);
    }
  }

  /**
   * Asserts that a bid was saved.
   *
   * @param int $nid
   *   ID of the auction node were a bid was placed for.
   * @param float $amount
   *   The expected amount that was bid.
   * @param int $bid_id
   *   (optional) ID of bid entity.
   *   Defaults to latest placed bid.
   */
  protected function assertPlaceBid($nid, $amount, $bid_id = NULL) {
    $amount_in_cents = $amount * 100;
    $amount_display = '$' . number_format($amount, 2, '.', '');

    if (is_null($bid_id)) {
      // Find the latest bid.
      $bid_id = db_query('SELECT MAX(id) FROM {eck_commerce_auction_bid}')->fetchField();
    }

    // Assert that the bid is saved.
    $bid = entity_load_single('commerce_auction_bid', $bid_id);
    $this->assertEqual($amount_in_cents, $bid->bid_amount[LANGUAGE_NONE][0]['amount'], format_string('The bid was placed with an amount of @amount (actual: @actual).', array(
      '@amount' => $amount_in_cents,
      '@actual' => $bid->bid_amount[LANGUAGE_NONE][0]['amount'],
    )));

    // Assert that the bid belongs to the given node.
    $node = node_load($nid, NULL, TRUE);
    $found = FALSE;
    foreach ($node->auction_bid_refs[LANGUAGE_NONE] as $key => $item) {
      if ($item['target_id'] == $bid_id) {
        $found = TRUE;
        break;
      }
    }
    $this->assertTrue($found, 'The bid belongs to the expected auction node.');

    // Assert that it is displayed as well.
    $this->assertText(t('Your bid is saved!'));
    $this->assertText($amount_display);
  }

}
