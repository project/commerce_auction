<?php

namespace Drupal\commerce_auction\Tests;

/**
 * Tests if bids can be made on auctions.
 *
 * @group commerce_auction
 */
class BidTest extends TestBase {
  /**
   * A Commerce product.
   *
   * @var object
   */
  protected $product;

  /**
   * A product display node.
   *
   * @var object
   */
  protected $display;

  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name' => 'Bid Test',
      'description' => 'Tests if bids can be made on auctions.',
      'group' => 'Commerce Auction',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setUp(array $modules = array(), array $permissions = array()) {
    parent::setUp($modules, $permissions);

    // Create product and display.
    $this->product = $this->createAuctionProduct();
    $this->display = $this->createAuctionProductDisplay($this->product->product_id);
  }

  /**
   * Basic test for placing a bid.
   */
  public function testPlaceBid() {
    // Login as user who may bid.
    $customer = $this->createUserWithPermissionHelper(array('store customer', 'auction customer'));
    $this->drupalLogin($customer);

    // Place a bid.
    $this->placeBid($this->display->nid, 60);
  }

  /**
   * Tests bid increment.
   */
  public function testBidIncrement() {
    // Set bid increment and minimum bid increase to 5.
    variable_set('commerce_auction_bid_increment', 5);
    variable_set('commerce_auction_min_bid_inc', 5);

    // Try to bid $12 more as the starting price. Should fail.
    $customer = $this->createUserWithPermissionHelper(array('store customer', 'auction customer'));
    $this->drupalLogin($customer);
    $this->placeBid($this->display->nid, 62, FALSE);
    // Assert that the bid was not saved.
    $this->assertText('Your bid amount should increase the current high bid ($50) by a multiple of 5 to be accepted.');
    $this->assertFalse(entity_load_single('commerce_auction_bid', 1));

    // Try to bid lower than the starting price with the right increment. Should fail.
    $this->placeBid($this->display->nid, 45, FALSE);
    $this->assertText('Your bid amount should be more than last bid amount ($50).');
    $this->assertFalse(entity_load_single('commerce_auction_bid', 1));

    // Now try to bid with the right increment.
    $this->placeBid($this->display->nid, 55);
  }

  /**
   * Tests minimum bid increase.
   */
  public function testMinimumBidIncrease() {
    // Set minimum bid increase to 10.
    variable_set('commerce_auction_min_bid_inc', 10);

    // Create a first bid as customer 1.
    $customer1 = $this->createUserWithPermissionHelper(array('store customer', 'auction customer'));
    $this->drupalLogin($customer1);
    $this->placeBid($this->display->nid, 60);

    // Now try to bid only $1 more as customer 2. Should fail.
    $customer2 = $this->createUserWithPermissionHelper(array('store customer', 'auction customer'));
    $this->drupalLogin($customer2);
    $this->placeBid($this->display->nid, 61, FALSE);
    // Assert that the bid was not saved.
    $this->assertText('Your bid amount must be at least $10 higher than the current high bid ($60) to be accepted.');
    $this->assertFalse(entity_load_single('commerce_auction_bid', 2));

    // Try to bid the start price (which is lower). Should fail.
    $this->placeBid($this->display->nid, 50, FALSE);
    // Assert that the bid was not saved.
    $this->assertText('Your bid amount should be more than last bid amount');
    $this->assertFalse(entity_load_single('commerce_auction_bid', 2));

    // Finally bid the right price higher.
    $this->placeBid($this->display->nid, 70);
  }

  /**
   * Tests maximum bid increase.
   */
  public function testMaximumBidIncrease() {
    // Set maximum bid increase to 20.
    variable_set('commerce_auction_max_bid_inc', 20);

    // Try to bid more as the maximum bid increase. Should fail.
    $customer = $this->createUserWithPermissionHelper(array('store customer', 'auction customer'));
    $this->drupalLogin($customer);
    $this->placeBid($this->display->nid, 90, FALSE);
    // Assert that the bid was not saved.
    $this->assertText('Your bid amount cannot be more than $20 higher than the current high bid ($50).');
    $this->assertFalse(entity_load_single('commerce_auction_bid', 1));

    // Now bid with the maximum bid increase. Should succeed.
    $this->placeBid($this->display->nid, 70);
  }

  /**
   * Tests maximum bid increase percentage.
   */
  public function testMaximumBidIncreasePercentage() {
    // Set maximum bid percentage to 100%.
    variable_set('commerce_auction_max_bid_inc', 100);

    // Try to bid more as the maximum bid increase percentage. Should fail.
    $customer = $this->createUserWithPermissionHelper(array('store customer', 'auction customer'));
    $this->drupalLogin($customer);
    $this->placeBid($this->display->nid, 120, FALSE);
    $this->assertText('Your bid should not increase the current high bid ($50) more than 100% of current highest bid ($50).');

    // Now bid with the maximum bid increase percentage. Should succeed.
    $this->placeBid($this->display->nid, 100);
  }

}
