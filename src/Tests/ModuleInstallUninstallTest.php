<?php

namespace Drupal\commerce_auction\Tests;

/**
 * Tests module installation and uninstallation.
 *
 * @group commerce_auction
 */
class ModuleInstallUninstallTest extends TestBase {

  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name' => 'Module installation and uninstallation',
      'description' => 'Tests if the Commerce Auction module can be installed and uninstalled without errors.',
      'group' => 'Commerce Auction',
    );
  }

  /**
   * Test installation and uninstallation.
   */
  protected function testInstallationAndUninstallation() {
    $this->assertTrue(module_exists('commerce_auction'));
    $this->assertTrue(module_exists('commerce_auction_lineitem'));
    $this->assertTrue(module_exists('commerce_auction_field_helper'));

    // @todo
    // Check availability of things.

    // Uninstall module.
    module_disable(array('commerce_auction', 'commerce_auction_lineitem', 'commerce_auction_field_helper'));
    drupal_uninstall_modules(array('commerce_auction', 'commerce_auction_lineitem', 'commerce_auction_field_helper'));
    $this->assertFalse(module_exists('commerce_auction'));
    $this->assertFalse(module_exists('commerce_auction_lineitem'));
    $this->assertFalse(module_exists('commerce_auction_field_helper'));

    // @todo
    // Check if all is properly cleaned up.
  }
}
